import { Component } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';




@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

admin: boolean;
gestion: boolean;

  form = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
  });

  bloquer=0;

  constructor(
    private authService: AuthenticationService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public http: HttpClient,
    private router: Router,
  ) { }



  async onSubmit() {

    const loading = await this.loadingCtrl.create({ message: 'Logging in ...' });
    await loading.present();

    this.http.post('http://localhost/userState/admin.php', this.form.value).subscribe(data => {
      this.http.post('http://localhost/userState/gestion.php', this.form.value).subscribe(response => {

     if(data && !response){
     this.admin=true;
     this.gestion=false;
     }

     else if(!data && response){
     this.admin=false;
     this.gestion=true;
     }

     this.authService.login(this.form.value,this.admin,this.gestion).subscribe(
      async token => {
        localStorage.setItem('token', token);
        loading.dismiss();
        this.http.post('http://localhost/userState/reset.php', this.form.value).subscribe(async result=> {

          if(result){

            const alert = await this.alertCtrl.create({
              header: 'Alert',
              subHeader: 'Compte Alert',
              message: 'Veiller changer de mot de passe',
              buttons: ['OK']
              });

              await alert.present();
            this.router.navigateByUrl('/menu/reset');
          }

          else{
            this.router.navigateByUrl('/menu/home');
          }

        }, error => {
         console.log(error);
                    });
      },


      async () => {
        if(this.bloquer<3){
        const alert = await this.alertCtrl.create({ message: 'Username or Password incorrect', buttons: ['OK'] });
        await alert.present();
        loading.dismiss();
        this.bloquer++;
        }

        if(this.bloquer>3){
        const alert = await this.alertCtrl.create({ message: 'Votre compte a été bloquer veiller contacter un administrateur',
         buttons: ['OK'] });
        await alert.present();
        this.bloquer=0;
        loading.dismiss();
        }

        if(this.bloquer===3){
          this.http.post('http://localhost/userState/lock.php', this.form.value).subscribe(data2=> {
          this.bloquer++;
        }, error => {
        console.log(error);
                   });
        }
      }
    );


      }, error => {
       console.log(error);
                  });
                }, error => {
                  console.log(error);
                             });
  }

}

