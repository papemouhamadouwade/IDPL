import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { User } from './user-delete';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.page.html',
  styleUrls: ['./user-delete.page.scss'],
})
export class UserDeletePage implements OnInit {

  user: User[];

  constructor(
    public service: ApiService,
    public http: HttpClient,
    private router: Router,

  ) { }

  ngOnInit() {
    this.service.getDataUser().subscribe(response => {
      this.user=response;
    });
  }

  delete(user){
    this.http.post('http://localhost/test/delete.php', user).subscribe(data => {
      this.service.getDataUser().subscribe(response => {
        this.user=response;
      });
      }, error => {
       console.log(error);
                  });
  }

  edit(user){
    this.router.navigateByUrl('/menu/user-modify');
  }

}
