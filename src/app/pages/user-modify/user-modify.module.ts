import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserModifyPageRoutingModule } from './user-modify-routing.module';

import { UserModifyPage } from './user-modify.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserModifyPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [UserModifyPage]
})
export class UserModifyPageModule {}
