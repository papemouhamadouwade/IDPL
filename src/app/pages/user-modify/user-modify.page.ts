import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { User } from '../user-delete/user-delete';

@Component({
  selector: 'app-user-modify',
  templateUrl: './user-modify.page.html',
  styleUrls: ['./user-modify.page.scss'],
})
export class UserModifyPage  {

  user: User[];

  form = new FormGroup({
    permission: new FormControl('', [
      Validators.required,
    ]),
    sexe: new FormControl('', [
      Validators.required,
    ]),
    direction: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'),
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
    usern: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
  });

  constructor(
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private router: Router,
    public http: HttpClient,
    public service: ApiService,
  ) { }



  async onSubmit() {
    const loading = await this.loadingCtrl.create({ message: 'Modifing...' });
    await loading.present();
      this.http.post('http://localhost/userState/reset_user.php', this.form.value).subscribe(
        // If success
        async () => {
          this.service.getDataUser().subscribe(response => {
            this.user=response;
          });
          const toast = await this.toastCtrl.create({ message: 'User modified', duration: 2000, color: 'dark' });
          await toast.present();
          loading.dismiss();
          this.form.reset();
          this.router.navigateByUrl('/menu/user-delete');
        },
        // If there is an error
        async () => {
          const alert = await this.alertCtrl.create({ message: 'There is an error', buttons: ['OK'] });
          loading.dismiss();
          await alert.present();
        }
      );
  }

}
