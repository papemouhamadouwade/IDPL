import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserModifyPage } from './user-modify.page';

const routes: Routes = [
  {
    path: '',
    component: UserModifyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserModifyPageRoutingModule {}
