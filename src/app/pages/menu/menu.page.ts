import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  navigate: any;
  isGestion: number;
  isadmin: boolean;
  isgestion: boolean;
  username: string;

  constructor(
   private authService: AuthenticationService,
    private router: Router,
  ) {
    this.sideMenu();
  }

  sideMenu() {
    this.authService.username.subscribe(response => {
    this.authService.admin.subscribe(data => {
      this.authService.gestion.subscribe(data2 => {
      if (data && !data2) {
          this.navigate =
          [
              {
              title : 'Changer mot de passe',
              url   : '/menu/profile',
              icon  : 'lock-closed'
              },
              {
                title : 'Modifier profil',
                url   : '/menu/user-edit',
                icon  : 'person'
                },
          ];
          this.username=response;
          this.isadmin=true;
          this.isgestion=false;
      }  else if (!data && data2){
        this.navigate =
        [
            {
            title : 'Changer mot de passe',
            url   : '/menu/profile',
            icon  : 'lock-closed'
            },
            {
              title : 'Modifier profil',
              url   : '/menu/user-edit',
              icon  : 'person'
              },
        ];
        this.username=response;
        this.isadmin=false;
        this.isgestion=true;
       }
       });
     });
  });

    }
    logout(){

        this.authService.logout();
        this.router.navigateByUrl('/login', { replaceUrl: true });
   }
   isAdmin(){
     return this.isadmin;
   }

   isGestionn(){
    return this.isgestion;
  }

   ngOnInit() {
  }

}

