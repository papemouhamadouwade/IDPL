import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth.guard';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'register',
        canActivate: [AuthGuard],
        loadChildren: () => import('../register/register.module').then(m => m.RegisterPageModule),
      },
      {
        path: 'deploque',
        canActivate: [AuthGuard],
        loadChildren: () => import('../deploque/deploque.module').then( m => m.DeploquePageModule)
      },
      {
        path: 'reset',
        canActivate: [AuthGuard],
        loadChildren: () => import('../reset/reset.module').then( m => m.ResetPageModule)
      },
      {
        path: 'home',
        canActivate: [AuthGuard],
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'profile',
        canActivate: [AuthGuard],
        loadChildren: () => import('../profile/profile.module').then( m => m.ProfilePageModule)
      },
      {
        path: 'user-delete',
        canActivate: [AuthGuard],
        loadChildren: () => import('../user-delete/user-delete.module').then( m => m.UserDeletePageModule)
      },
      {
        path: 'user-edit',
        canActivate: [AuthGuard],
        loadChildren: () => import('../user-edit/user-edit.module').then( m => m.UserEditPageModule)
      },
      {
        path: 'salle-create',
        canActivate: [AuthGuard],
        loadChildren: () => import('../salle-create/salle-create.module').then( m => m.SalleCreatePageModule)
      },
      {
        path: 'salle-edit',
        canActivate: [AuthGuard],
        loadChildren: () => import('../salle-edit/salle-edit.module').then( m => m.SalleEditPageModule)
      },
      {
        path: 'user-modify',
        canActivate: [AuthGuard],
        loadChildren: () => import('../user-modify/user-modify.module').then( m => m.UserModifyPageModule)
      },
      {
        path: 'reunion-week',
        canActivate: [AuthGuard],
        loadChildren: () => import('../reunion-week/reunion-week.module').then( m => m.ReunionWeekPageModule)
      },
      {
        path: 'salle-modify',
        canActivate: [AuthGuard],
        loadChildren: () => import('../salle-modify/salle-modify.module').then( m => m.SalleModifyPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
