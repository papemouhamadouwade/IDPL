import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalleEditPageRoutingModule } from './salle-edit-routing.module';

import { SalleEditPage } from './salle-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SalleEditPageRoutingModule
  ],
  declarations: [SalleEditPage]
})
export class SalleEditPageModule {}
