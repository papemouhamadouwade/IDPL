import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalleEditPage } from './salle-edit.page';

const routes: Routes = [
  {
    path: '',
    component: SalleEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalleEditPageRoutingModule {}
