import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { Salles } from './salle-edit';

@Component({
  selector: 'app-salle-edit',
  templateUrl: './salle-edit.page.html',
  styleUrls: ['./salle-edit.page.scss'],
})
export class SalleEditPage implements OnInit {

  salle: Salles[];

  constructor(
    public service: ApiService,
    public http: HttpClient,
    public alertCtrl: AlertController,
    private router: Router,
  ) { }

  ngOnInit() {
    this.service.getDataSalle().subscribe(response => {
      this.salle=response;
    });
  }

  delete(salles){
    this.http.post('http://localhost/salle/delete.php', salles).subscribe(data => {
      this.service.getDataSalle().subscribe(response => {
        this.salle=response;
      });
      }, error => {
       console.log(error);
                  });
  }

  async edit(salles){
    this.router.navigateByUrl('/menu/salle-modify');
  }

}
