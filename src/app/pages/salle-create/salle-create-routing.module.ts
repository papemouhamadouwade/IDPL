import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalleCreatePage } from './salle-create.page';

const routes: Routes = [
  {
    path: '',
    component: SalleCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalleCreatePageRoutingModule {}
