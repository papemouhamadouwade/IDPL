import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalleCreatePageRoutingModule } from './salle-create-routing.module';

import { SalleCreatePage } from './salle-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SalleCreatePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SalleCreatePage]
})
export class SalleCreatePageModule {}
