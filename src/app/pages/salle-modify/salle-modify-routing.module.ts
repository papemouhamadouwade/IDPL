import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalleModifyPage } from './salle-modify.page';

const routes: Routes = [
  {
    path: '',
    component: SalleModifyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalleModifyPageRoutingModule {}
