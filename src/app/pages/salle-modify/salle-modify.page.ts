import { HttpClient } from '@angular/common/http';
import { Component} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { Salles } from '../salle-edit/salle-edit';

@Component({
  selector: 'app-salle-modify',
  templateUrl: './salle-modify.page.html',
  styleUrls: ['./salle-modify.page.scss'],
})
export class SalleModifyPage {

  salle: Salles[];


  dates= new Date();
  defaultDate: string =new Date().toISOString();

  form = new FormGroup({
    nsalle: new FormControl ('', [
      Validators.required,
      Validators.pattern('^[0-9]+$')
    ]),
    nsalle2: new FormControl ('', [
      Validators.required,
      Validators.pattern('^[0-9]+$')
    ]),
    nplace: new FormControl ('', [
      Validators.required,
      Validators.pattern('^[0-9]+$')
    ]),
    heure: new FormControl ('', [
      Validators.required,
      //Validators.pattern('^[0-9]+-[0-9]+$'),
      //Validators.minLength(5),
    ]),
    heure2: new FormControl ('', [
      Validators.required,
      //Validators.pattern('^[0-9]+-[0-9]+$'),
      //Validators.minLength(5),
    ]),
    date2: new FormControl ('', [
      Validators.required,
    ]),
  });


  newDate =new Date(this.form.value.heure).getHours();
  newDate2 =new Date(this.form.value.heure2).getHours();



  constructor(
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public http: HttpClient,
    public service: ApiService,
    private router: Router) { }




  getDate(e) {
    const date = new Date(e.target.value).toISOString().substring(0, 10);
    this.form.get('date2').setValue(date, {
      onlyself: true
    });
  }





  async submitForm() {
    const loading = await this.loadingCtrl.create({ message: 'modifing...' });
    await loading.present();
    this.http.post('http://localhost/salle/reset_salle.php', this.form.value).subscribe(
      // If success
      async () => {
        this.service.getDataSalle().subscribe(response => {
          this.salle=response;
        });
        const toast = await this.toastCtrl.create({ message: 'Salle modified', duration: 2000, color: 'dark' });
        await toast.present();
        loading.dismiss();
        this.form.reset();
        this.router.navigateByUrl('/menu/salle-edit');
      },
      // If there is an error
      async () => {
        const alert = await this.alertCtrl.create({ message: 'There is an error', buttons: ['OK'] });
        loading.dismiss();
        await alert.present();
      }
    );
    console.log(this.form.value.heure);
   }
  }
