import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalleModifyPageRoutingModule } from './salle-modify-routing.module';

import { SalleModifyPage } from './salle-modify.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SalleModifyPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SalleModifyPage]
})
export class SalleModifyPageModule {}
