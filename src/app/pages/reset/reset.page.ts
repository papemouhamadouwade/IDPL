import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.page.html',
  styleUrls: ['./reset.page.scss'],
})
export class ResetPage {

  form = new FormGroup({
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}'),
    ]),
    repassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });
  constructor(
    private loadingCtrl: LoadingController,
    public http: HttpClient,
    private router: Router,
   private authService: AuthenticationService,
  ) { }


  async onSubmit() {
    const loading = await this.loadingCtrl.create({ message: 'Reset...' });
    await loading.present();
    this.authService.username.subscribe(data => {
      const postData = {
        username:  data,
        password: this.form.value.password,
                       };
                       this.http.post('http://localhost/userState/reset_success.php', postData).subscribe(response=> {
        loading.dismiss();
        this.router.navigateByUrl('/menu/home');
    }, error => {
       console.log(error);
                  });
    });

  }
}

