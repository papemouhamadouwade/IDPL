import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { Salles } from '../salle-edit/salle-edit';

@Component({
  selector: 'app-reunion-week',
  templateUrl: './reunion-week.page.html',
  styleUrls: ['./reunion-week.page.scss'],
})
export class ReunionWeekPage implements OnInit {

  salle: Salles[];

  constructor(
    public service: ApiService,
    public http: HttpClient,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.service.getDataReunion().subscribe(response => {
      this.salle=response;
    });
  }

}
