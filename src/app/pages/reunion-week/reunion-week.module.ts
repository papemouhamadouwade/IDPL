import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReunionWeekPageRoutingModule } from './reunion-week-routing.module';

import { ReunionWeekPage } from './reunion-week.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReunionWeekPageRoutingModule
  ],
  declarations: [ReunionWeekPage]
})
export class ReunionWeekPageModule {}
