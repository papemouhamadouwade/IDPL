import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReunionWeekPage } from './reunion-week.page';

const routes: Routes = [
  {
    path: '',
    component: ReunionWeekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReunionWeekPageRoutingModule {}
