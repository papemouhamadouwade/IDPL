import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeploquePage } from './deploque.page';

const routes: Routes = [
  {
    path: '',
    component: DeploquePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeploquePageRoutingModule {}
