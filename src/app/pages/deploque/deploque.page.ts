import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController} from '@ionic/angular';

@Component({
  selector: 'app-deploque',
  templateUrl: './deploque.page.html',
  styleUrls: ['./deploque.page.scss'],
})
export class DeploquePage {

  form = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ])
  });

  constructor(
    private loadingCtrl: LoadingController,
    public http: HttpClient,
    private router: Router
  ) { }


  async onSubmit() {
    const loading = await this.loadingCtrl.create({ message: 'Unlock...' });
    await loading.present();

      this.http.post('http://localhost/userState/unlock.php', this.form.value).subscribe(data=> {
        loading.dismiss();
        this.router.navigateByUrl('/menu/home');
    }, error => {
       console.log(error);
                  });

  }

}
