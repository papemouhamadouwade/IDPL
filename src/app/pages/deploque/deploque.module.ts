import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeploquePageRoutingModule } from './deploque-routing.module';

import { DeploquePage } from './deploque.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeploquePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [DeploquePage]
})
export class DeploquePageModule {}
