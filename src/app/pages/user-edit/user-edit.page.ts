import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.page.html',
  styleUrls: ['./user-edit.page.scss'],
})
export class UserEditPage  {

  form = new FormGroup({
    direction: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}'),
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
    oldusername: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
  });
  constructor(
    public http: HttpClient,
  ) { }

  onSubmit() {
    this.http.post('http://localhost/userState/reset_user.php', this.form.value).subscribe(datat => {
      }, error => {
       console.log(error);
                  });
  }

}
