import { Salles } from './../pages/salle-edit/salle-edit';
import { User } from './../pages/user-delete/user-delete';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';





@Injectable({
  providedIn: 'root'
})
export class ApiService {



public url='http://localhost/test/retrieve_data_user.php';
public url2='http://localhost/salle/retrieve_data_salle.php';
public url3='http://localhost/salle/retrieve_data_reunion.php';



  constructor(private http: HttpClient) { }


  getDataUser(){
    return this.http.get<[User]>(this.url);
    }

    getDataSalle(){
      return this.http.get<[Salles]>(this.url2);
      }

      getDataReunion(){
        return this.http.get<[Salles]>(this.url3);
        }

}
