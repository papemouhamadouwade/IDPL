import { User, UserProfile } from './user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { BehaviorSubject} from 'rxjs/';
import { Platform } from '@ionic/angular';



@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  redirectUrl: string;
  currentUser: UserProfile;
  admin = new BehaviorSubject(false);
  gestion = new BehaviorSubject(false);
  username: BehaviorSubject<string>;
  authenticationState = new BehaviorSubject(false);
    private url = 'http://localhost/auth_app/api';
  constructor(private http: HttpClient ,private plt: Platform) {
    this.plt.ready().then(() => {
      this.checkToken();
    });

  }

  register(user: User) {
    return this.http.post(`${this.url}/register`, user);
  }



  checkToken() {
      if (localStorage.getItem('token')) {
        this.authenticationState.next(true);
      }
  }

  login(credentials: User,admin: boolean,gestion: boolean): Observable<string> {
    if (admin && !gestion){
      this.currentUser = {
        username: credentials.username,
        role: 0

      };
      this.admin.next(true);
      this.gestion.next(false);
      this.authenticationState.next(true);
      this.username=new BehaviorSubject(credentials.username);
      this.username.next(credentials.username);
    }
    else if (!admin && gestion){
      this.currentUser = {
        username: credentials.username,
        role: 1

      };
      this.gestion.next(true);
      this.admin.next(false);
      this.username=new BehaviorSubject(credentials.username);
      this.username.next(credentials.username);
      this.authenticationState.next(true);
    }
    return this.http.post<{ token: string }>(`${this.url}/login`, credentials).pipe(
      map(response => response.token)
      );
  }


logout(){

   localStorage.removeItem('token');
    this.authenticationState.next(false);
  return this.authenticationState.value;


}
isAdmin(){
  return this.admin.value;

}

isGestion(){
  return this.gestion.value;

}

isAuthenticated() {
  return this.authenticationState.value;
 }
}
