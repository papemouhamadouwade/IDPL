
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
   },
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'user-modify',
    loadChildren: () => import('./pages/user-modify/user-modify.module').then( m => m.UserModifyPageModule)
  },
  {
    path: 'reunion-week',
    loadChildren: () => import('./pages/reunion-week/reunion-week.module').then( m => m.ReunionWeekPageModule)
  },
  {
    path: 'salle-modify',
    loadChildren: () => import('./pages/salle-modify/salle-modify.module').then( m => m.SalleModifyPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
