<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods:POST,GET,PUT,DELETE');
header('Access-Control-Allow-Headers: content-type or other');
header('Content-Type: application/json');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once './dbh.php';


$formData = json_decode(file_get_contents('php://input'));
foreach ($formData as $key=>$value) {
    $_POST[$key]=$value;
}
if (isset($_POST['firstDate'] )) {
$firstDate=$_POST['firstDate'];
$secondDate= $_POST['secondDate'];

  $data    = array();

try {   
 $stmt= $conn->query("SELECT  CAST(DATE_DEMANDE AS DATE) name, 100.0 * count(NUMEROR_DEMANDE) / (SELECT sum(nbr) total from (SELECT count(*) nbr FROM demande where VALIDE='O' and CAST(DATE_DEMANDE AS DATE) between '".$firstDate."' and '".$secondDate."' group by CAST(DATE_DEMANDE AS DATE)) as y) y FROM demande where VALIDE='O' and CAST(DATE_DEMANDE AS DATE) between '".$firstDate."' and '".$secondDate."' group by CAST(DATE_DEMANDE AS DATE) order by CAST(DATE_DEMANDE AS DATE) ASC");
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $data[] = $row;
      }

      // Return data as JSON
      echo json_encode($data,JSON_NUMERIC_CHECK );
  }
      catch(PDOException $e)
   {
      echo $e->getMessage();
   }

}



