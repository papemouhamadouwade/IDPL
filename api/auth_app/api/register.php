<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
 
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
 
include_once 'config/dbh.php';
  
   if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $data = json_decode(file_get_contents("php://input"));
    $uname = $data->username;
    $pass = $data->password;
    $perm = $data->permission;
    $sexe = $data->sexe;
    $direction = $data->direction;
    $email = $data->email;
 

$hashed = password_hash($pass, PASSWORD_DEFAULT);

if($perm==1){
    if($sexe==1){
       $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,sexe,direction,email) VALUES ('$uname', '$hashed','Agent','M','$direction','$email')");
    }
    if($sexe==2){
        $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,sexe,direction,email) VALUES ('$uname', '$hashed','Agent','F','$direction','$email')");
    }
}

else if($perm==2){
    if($sexe==1){
       $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,sexe,direction,email) VALUES ('$uname', '$hashed','System','M','$direction','$email')");
    }
    if($sexe==2){
        $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,sexe,direction,email) VALUES ('$uname', '$hashed','System','F','$direction','$email')");
    }
}

else if($perm==3){
    if($sexe==1){
       $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,sexe,direction,email) VALUES ('$uname', '$hashed','Gestionnaire','M','$direction','$email')");
    }
    if($sexe==2){
        $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,sexe,direction,email) VALUES ('$uname', '$hashed','Gestionnaire','F','$direction','$email')");
    }
}

else if($perm==4){
    if($sexe==1){
       $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,expire,Sexe,Direction,Email) VALUES ('$uname', '$hashed','Administrateur','N','M','$direction','$email')");
    }
    if($sexe==2){
        $sql = $conn->query("INSERT INTO utilisateurs (username, password,profil,expire,sexe,direction,email) VALUES ('$uname', '$hashed','Administrateur','N','F','$direction','$email')");
    }
}

    if ($sql) {
        http_response_code(201);
        echo json_encode(array('message' => 'User created'));
    } else {
        http_response_code(500);
        echo json_encode(array('message' => 'Internal Server error'));
    }
} else {
    http_response_code(404);
}
 
?>